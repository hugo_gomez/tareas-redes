# Universidad Nacional Autónoma de México
## Facultad de ciencias
### Redes de computadoras
### Practica 6
### Hugo Alberto Gomez Gutierrez
### 310054236
### Mijail Aron Alvarez Cerrillo 
### 310020590

#### Instalacion apache
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/instalacio%20apache2.png)
#### Parar, iniciar y habilitar apache2
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/parar,%20iniciar%20y%20habilitar%20apache2.png)
#### Probando Apache
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/probando%20apache2.png)
#### Instalando Mariadb cliente y server
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/instalacion%20mariadb-client.png)
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/instalacion%20mariadb-server.png)
#### Parar, iniciar y habilitar mariaDB
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/creando%20una%20contrase%C3%B1a%20de%20root%20y%20deshabilitando%20el%20acceso%20de%20root%20remoto.png)
#### Instalando PHP
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/instalando%20php%20y%20modulos.png)
#### Configurar archivo para apache2
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/configurar%20php%20y%20apache.png)
#### Restableces apache2
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/restablecer%20apache2.png)
#### Creando archivo phpinfo.php
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/probar%20la%20configuraci%C3%B3n%20de%20PHP%207.2%20con%20Apache2.png)
Se agrega la linea <?php phpinfo( ); ?> al archivo.
#### Ver pagina de prueba de php
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/ver%20la%20p%C3%A1gina%20de%20prueba%20predeterminada%20de%20PHP.png)
#### Creando base de datos
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/Captura%20de%20pantalla%20de%202019-05-12%2021-47-12.png)
#### Descargando drupal
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/descargar%20drupal.png)
#### Preparamos apache2 para la instalación
Agregamos el siguiente contenido en el archivo  `000-default.conf`

< IfModule mod_rewrite.c>
	 RewriteEngine On
</	IfModule>
< Directory /var/www/html/>
	 Options Indexes FollowSymLinks
	 AllowOverride All
	 Require all granted
 < /Directory>
 #### Restaurar apache2.service
 ![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/restablecer%20apache2.png)
 #### Abrimos drupal en el navegador
 ![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/Captura%20de%20pantalla%20de%202019-05-12%2021-00-05.png)
 #### Seleccionamos la base de datos
 ![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/Captura%20de%20pantalla%20de%202019-05-12%2021-00-52.png)
 #### Se instala drupal
 ![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/Captura%20de%20pantalla%20de%202019-05-12%2021-16-58.png)
 #### Configuramos el sitio
 ![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/Captura%20de%20pantalla%20de%202019-05-12%2021-43-38.png)
 #### Ya tenemos drupal instalado en el servidor
 ![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p6/imagenes/Captura%20de%20pantalla%20de%202019-05-12%2021-46-37.png)
 
 

